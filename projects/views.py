from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_view_projects(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/project_list.html", context)


@login_required
def detail_view_projects(request, id):
    project_detail = Project.objects.get(id=id)
    context = {"project_detail": project_detail}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_view_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.purchaser = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm
    context = {"form": form}

    return render(request, "projects/create_project.html", context)
