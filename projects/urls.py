from django.urls import path
from projects.views import (
    list_view_projects,
    detail_view_projects,
    create_view_project,
)

urlpatterns = [
    path("", list_view_projects, name="list_projects"),
    path("<int:id>/", detail_view_projects, name="show_project"),
    path("create/", create_view_project, name="create_project"),
]
