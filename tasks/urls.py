from django.urls import path
from tasks.views import create_task, list_view_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", list_view_tasks, name="show_my_tasks"),
]
